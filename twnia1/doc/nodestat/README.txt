Handy command scripts for HPC cluster users accessing PBS Pro Job Scheduler. 
These scripts will provide users with a more friendly overview of online jobs and CPU/GPU resources allocation.


Requirements: Altair PBS Pro (pbs_version = 14.2.4) installed in your HPC cluster.



Target User: System Administrator         



Installation: 

1. copy these scripts to /usr/local/bin/ on your cluster login nodes.

   cp nodestat /usr/local/bin/
   cp pqueues /usr/local/bin/
   cp jobstat /usr/local/bin/
   
2. add executable attributes to these scripts.
  
   cd /usr/local/bin/
   chmod a+x nodestat
   chmod a+x pqueues
   chmod a+x jobstat

 
Note: If your PBS Pro has been installed to other path (default is /opt/pbs/), 
      please modify QSTAT or PBSNODE variables directly in scripts.