說明:
1. 它是由好幾個 slurm commands 組成的
執行 qq 後會出現三部份的資訊
上半部是 squeue
image1.png
中段是 sinfo
image2.png
下半部是 user jobs 的資訊，像是這樣
image3.png

2. 上半部的 squeue 結果會列出一長串，user 的名字有 highlight 起來，方便尋找

3. 下半部會看到user擁有的jobs，有上編號 #
若是要跳到 job #，用指令  . cd#  (注意，前面有一個"點")
若是要刪除 job #，用指令  . qd#  (注意，前面有一個"點")

4. qq運作的過程，會在 ~/bin 產生一個目錄 q-path
在執行 qq 時把下半段的資訊儲存在 ~/bin/q-path/list 檔
(USER 可以透過這個 list 來找出以前JOB的所在目錄)
並且把 cd# & qd# 寫到這個 q-path 裡面
如此一來就可以使用 . cd# & . qd#
不過前提是要在 PATH 裡加入 ~/bin/q-path
